package org.itboy.study.springcloud.config;

import java.util.Date;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author cjb
 * @createTime 2021/3/8
 */
@SpringBootApplication(scanBasePackages = {"org.itboy.study"})
@EnableConfigServer
@RequestMapping
public class ConfigCenterApp {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ConfigCenterApp.class, args);
        System.out.println(context.getClass().getName());
    }

    @GetMapping("/test")
    @ResponseBody
    public Object test() {
        return new Date();
    }

}
